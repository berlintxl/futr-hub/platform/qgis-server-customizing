# Custom QGIS Server Docker Image

This project builds a custom Docker image of the [QGIS](https://www.qgis.org/en/site/) Server, since there is no official image in any public Docker registry.

It is based upon the Dockerfile, described in [chapter 7](https://docs.qgis.org/3.16/en/docs/server_manual/containerized_deployment.html) of the [QGIS documentation](https://docs.qgis.org/3.16/en/docs/index.html).

The image is built automatically, tagged as `webgis-qgisserver` and pushed into the GitLab registry by using `.gitlab-ci.yml`.


# Git Pre-Commit-Hook

The file `pre-commit.sh` is a Git hook, that will be executed before you commit changes to this repo.
It removes sensitive data from the file `project.qgs`, before it will be commited.

To install, simply run the following command in your Shell.
```bash
git clone git@gitlab.com:berlintxl/futr-hub/platform/qgis-server-customizing.git
cd qgis-server-customizing
chmod +x pre-commit.sh
ln -s ../../pre-commit.sh .git/hooks/pre-commit
```

## License
This work is licensed under [EU PL 1.2](LICENSE) by the State of Berlin, Germany, represented by [Tegel Projekt GmbH](https://www.tegelprojekt.de/). Please see the [list of authors](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/AUTHORS-ATTRIBUTION.md) and [list of contributors](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/LIST-OF-CONTRIBUTORS.md).

All contributions to this repository from January 1st 2020 on are considered to be licensed under the EU PL 1.2 or any later version.
This project doesn't require a CLA (Contributor License Agreement). The copyright belongs to all the individual contributors. For further information, please see the [guidelines for contributing](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/CONTRIBUTING.md).
