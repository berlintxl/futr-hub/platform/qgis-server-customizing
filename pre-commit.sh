#!/bin/sh

# Perform the followinbg string replacements in file project.qgs:
#
# - s/dbname=.* host=.* port=[0-9]*/service='qwc_geodb'/g
# - s/authcfg=[0-9a-zAZ]*//g

# Where to find project.qgs?
REPO_PATH=$(git rev-parse --show-toplevel --sq)
PROJECT_QGS="${REPO_PATH}/project.qgs"
PG_SERVICE_CONF='qwc_geodb'

if [ ! -f "${PROJECT_QGS}" ]; then
  echo "Could not find file project.qgs"
  exit 1
fi

# Perform string replacement.
sed -i "s/user='[^\']*'\s\{0,1\}//g" "${PROJECT_QGS}"
sed -i "s/password='[^\']*'\s\{0,1\}//g" "${PROJECT_QGS}"
sed -i "s/dbname=.* host=.* port=[0-9]*/service='${PG_SERVICE_CONF}'/g" "${PROJECT_QGS}"
sed -i "s/authcfg=[0-9a-zAZ]*//g" "${PROJECT_QGS}"


# (Re-)Add project.qgs file
git add "${PROJECT_QGS}"


# Paranoia check
grep -e "user" -e "password" -e "dbname=" -e "host=" -e "port=" -e "authcfg=" "${PROJECT_QGS}"
PARANOIA=$?
if [ $PARANOIA -ne 0 ]; then
  exit 0
else
  echo "ERROR: some replacements were unsuccessful."
  exit 1
fi

